﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiApplication.Data;
using WebApiApplication.Models;

namespace WebApiApplication.Controllers
{
    [Route("api/[controller]")]
    public class BooksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BooksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET api/values
        [HttpGet]
        public List<Book> Get()
        {
            return _context.Books.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Book Get(int id)
        {
            var book = _context.Books.FirstOrDefault(x => x.Id == id);
            if (book == null)
                NotFound();

            return book;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Book value)
        {
            _context.Books.Add(value);
            _context.SaveChanges();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Book value)
        {
            var book = _context.Books.FirstOrDefault(x => x.Id == id);
            if (book == null)
                NotFound();

            book.Name = value.Name;
            book.Author = value.Author;

            _context.Books.Update(book);
            _context.SaveChanges();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var book = _context.Books.FirstOrDefault(x => x.Id == id);
            if (book == null)
                NotFound();

            _context.Books.Remove(book);
            _context.SaveChanges();
        }
    }
}